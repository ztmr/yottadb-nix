{ pkgs ? import <nixpkgs> { } }:

{
  yottadb  = pkgs.callPackage ./yottadb.nix {};
}

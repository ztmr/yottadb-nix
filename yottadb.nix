{
  lib, gcc10Stdenv, fetchgit, pkgs
}:

# NOTE: The last stable release r1.30 does not build on lke_open declaration mismatch...
let ydbRelease = "1.31";
    ydbCommitRef = "cbd3669951004499b3574700af75dedd9a248ea3";
    ydbPkgSha256 = "1bvxrg8cmq7chk0v4ab52qy72lf94yc82mn45l3zbmm9lj7b81s4";
in gcc10Stdenv.mkDerivation rec {
  pname = "yottadb";
  version = "${ydbRelease}-git-${ydbCommitRef}";
  rev = "${ydbCommitRef}";

  src = fetchgit {
    leaveDotGit = true;
    url = "https://gitlab.com/YottaDB/DB/YDB.git";
    rev = "${rev}";
    sha256 = "${ydbPkgSha256}";
  };

  nativeBuildInputs = with pkgs; [
    file cmake gnumake curl tcsh
    libelf libconfig libgcrypt libgpgerror
    gpgme ncurses openssl zlib binutils
    pkg-config icu git
  ];

  buildInputs = with pkgs; [
    file libelf ncurses binutils locale
    pkg-config icu
  ];

  configurePhase = ''
    echo "CONFIGURE: $(pwd)"
    mkdir build && cd build
    cmake \
      -DCMAKE_INSTALL_PREFIX:PATH=$PWD \
      -DYDB_INSTALL_DIR:STRING=ydbkit \
      ../
  '';

  buildPhase = ''
    echo "BUILD: $(pwd)"
    # Fix missing ../.git/index
    git reset
    # Make sure that we can find all the runtime libs in order to
    # use the freshly built M compiler to build the M code...
    export LD_LIBRARY_PATH="$(pkg-config --variable libdir icu-io ncurses libelf | tr ' ' ':'):$LD_LIBRARY_PATH"
    echo $LD_LIBRARY_PATH
    # Build everything
    make -j16 all
    make install
  '';

  installPhase = ''
    echo "INSTALL: $(pwd)"
    # XXX: Non-root install is not supported
    #cd ydbkit && ./ydbinstall --installdir "$out"
    # XXX: 'configure' is not working as a non-root, so we cannot use it

    # Fake the installation
    rm -f $out ; mv ydbkit $out ; cd $out

    # Make binaries available in the NixPkg's bin dir
    mkdir $out/bin ; cd $out/bin ; find $out/ -mindepth 1 -maxdepth 1 -type f -executable -exec ln -rfs {} $out/bin/ \;
    rm -f $out/bin/configure

    # XXX: segmentation fault!!!
    #cd $out ; ./yottadb *.m
  '';

  enableParallelBuilding = true;

  meta = with lib; {
    description = "A proven Multi-Language NoSQL database engine";
    homepage = "https://gitlab.com/YottaDB/DB/YDB";
    license = licenses.agpl3;
    maintainers = [ ztmr ];
    platforms = platforms.all;
  };
}
